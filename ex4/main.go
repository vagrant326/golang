package main

import(
	"golang.org/x/net/html"
	"os"
	"fmt"
	"strings"
)

var links []Link

func main() {
	//var links []Link
	file, err := os.Open("tests/test4.html")
	defer file.Close()

	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	doc, err := html.Parse(file)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	node := doc.FirstChild
	for node != nil{
		parseNodes(node)
		node = node.NextSibling
	}
	printLinks()
}

func parseNodes(node *html.Node){
	if node.Type == html.ElementNode && node.Data == "a" {
		var link Link
		for _, attribute := range node.Attr{
			if attribute.Key == "href" {
				link.Href = attribute.Val
			}
		}
		link.Text = getStringValue(node.FirstChild)
		links = append(links, link)
	} else {
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			parseNodes(child)
		}
	}
}

func printLinks(){
	for index, link := range links{
		fmt.Printf("%v. [%s]: %s\r\n", index+1, link.Href, link.Text)
	}
}

func getStringValue(node *html.Node) string{
	text := ""
	for node != nil{
		if node.Type == html.TextNode{
			text += strings.TrimSpace(node.Data + " ")
		} else {
			text += getStringValue(node.FirstChild)
		}
		node = node.NextSibling
	}
	return text
}
