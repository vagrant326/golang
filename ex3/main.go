package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
)

func main() {
	//mux := defaultMux()
	json, _ := ioutil.ReadFile("gopher.json")
	story := ParseJSONStory(json)

	consoleGame(story)

	//jsonHandler , err := JSONHandler(story, mux)
	//if err != nil {
	//	fmt.Println(err)
	//	panic(err)
	//}
	//fmt.Println("Starting the server on :8080")
	//http.ListenAndServe(":8080", jsonHandler)
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}