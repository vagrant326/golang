package main

import (
	"fmt"
	"strconv"
)

func consoleGame(story map[string]Page){
	chapter:="intro"
	for{
		printPage(story[chapter])
		if len(story[chapter].Options)>0{
			choice := printOptions(story[chapter].Options)
			chapter = story[chapter].Options[choice-1].Arc
		} else {
			break
		}
	}
}

func printPage(page Page){
	fmt.Println(page.Title)
	fmt.Println()
	for _, text := range page.Story{
		fmt.Println(text)
	}
}

func printOptions(options []Option) int{
	fmt.Println()
	fmt.Println("What do You want to do?")
	for index, option := range options{
		fmt.Printf("%v. %s\t\t", index+1, option.Text)
	}
	fmt.Println()
	var input string
	fmt.Scanln(&input)
	result, _ := strconv.Atoi(input)
	return result
}
