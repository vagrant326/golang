package main

import (
	"net/http"
	"context"
	"html/template"
)

func JSONHandler(storyMap map[string]Page, fallback http.Handler) (http.HandlerFunc, error) {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), "app.req.id", "12345")
		link := r.URL.Path[len("/"):]
		if val, ok := storyMap[link]; ok {
			t, _ := template.ParseFiles("template.html")
			t.Execute(w, val)
		} else {
			fallback.ServeHTTP(w, r.WithContext(ctx))
			return
		}
	}
	return fn, nil
}
