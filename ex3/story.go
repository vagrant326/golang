package main

import "encoding/json"

type Story struct{
	Pages map[string]Page
}

type Page struct{
	Title string `json:"title"`
	Story []string `json:"story"`
	Options []Option `json:"options"`
}

type Option struct{
	Text string `json:"text"`
	Arc string `json:"arc"`
}

func ParseJSONStory (jsn []byte) map[string]Page{
	pagesMap := make(map[string]Page)
	json.Unmarshal(jsn, &pagesMap)
	return pagesMap
}