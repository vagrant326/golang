package main

import (
	"fmt"
	"net/http"
	"ex2"
	"flag"
	"io/ioutil"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	var yamlFile string
	flag.StringVar(&yamlFile, "yamlFile", "default.yaml", "Adres wlasnego pliku z adresami")
	flag.Parse()
	mux := defaultMux()

	// Build the MapHandler using the mux as the fallback
	pathsToUrls := map[string]string{
		"/urlshort-godoc": "https://godoc.org/github.com/gophercises/urlshort",
		"/yaml-godoc":     "https://godoc.org/gopkg.in/yaml.v2",
	}
	mapHandler := ex2.MapHandler(pathsToUrls, mux)

	// Build the YAMLHandler using the mapHandler as the
	// fallback
	yaml, _ := ioutil.ReadFile(yamlFile)

	yamlHandler, err := ex2.YAMLHandler(yaml, mapHandler)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	json, _ := ioutil.ReadFile("default.json")
	jsonHandler , err := ex2.JSONHandler(json, yamlHandler)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	db, err := sql.Open("sqlite3", "./database.db")
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	//initDB(db)
	dbHandler, _ := ex2.DBHandler(db, jsonHandler)

	fmt.Println("Starting the server on :8080")
	http.ListenAndServe(":8080", dbHandler)
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}

func initDB(db *sql.DB){
	statement, _ := db.Prepare("CREATE TABLE IF NOT EXISTS links (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, path NVARCHAR(511), url NVARCHAR(511))")
	statement.Exec()
	statement, _ = db.Prepare("INSERT INTO links (path, url) VALUES (?, ?)")
	statement.Exec("/db", "https://www.ath.bielsko.pl")
	statement.Exec("/sqlite", "https://github.com/mattn/go-sqlite3")
}
