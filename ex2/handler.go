package ex2
import (
	"net/http"
	//"fmt"
	"context"
	"gopkg.in/yaml.v2"
	"encoding/json"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

type Link struct{
	Path string
	Url string
}

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request){
		link := r.URL.Path
		ctx := context.WithValue(r.Context(), "app.req.id", "12345")
		if val, ok := pathsToUrls[link]; ok {
			http.Redirect(w, r, val, 303)
			return
		} else {
			fallback.ServeHTTP(w, r.WithContext(ctx))
			return
		}
	}
	return fn
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
func YAMLHandler(yml []byte, fallback http.Handler) (http.HandlerFunc, error) {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), "app.req.id", "12345")
		link := r.URL.Path

		var linksArray []Link
		yaml.Unmarshal(yml, &linksArray)

		linksMap := make(map[string]string)
		for _, value := range linksArray{
			linksMap[value.Path] = value.Url
		}

		if val, ok := linksMap[link]; ok {
			http.Redirect(w, r, val, 303)
			return
		} else {
			fallback.ServeHTTP(w, r.WithContext(ctx))
			return
		}
	}
	return fn, nil
}

func JSONHandler(jsn []byte, fallback http.Handler) (http.HandlerFunc, error) {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), "app.req.id", "12345")
		link := r.URL.Path

		var linksArray []Link
		json.Unmarshal(jsn, &linksArray)

		linksMap := make(map[string]string)
		for _, value := range linksArray{
			linksMap[value.Path] = value.Url
		}

		if val, ok := linksMap[link]; ok {
			http.Redirect(w, r, val, 303)
			return
		} else {
			fallback.ServeHTTP(w, r.WithContext(ctx))
			return
		}
	}
	return fn, nil
}

func DBHandler(db *sql.DB, fallback http.Handler) (http.HandlerFunc, error) {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), "app.req.id", "12345")
		link := r.URL.Path

		statement, _ := db.Prepare("SELECT url FROM links WHERE path LIKE ?")
		var url string
		err := statement.QueryRow(link).Scan(&url)
		if err == nil {
			http.Redirect(w, r, url, 303)
			return
		} else {
			fallback.ServeHTTP(w, r.WithContext(ctx))
			return
		}
	}
	return fn, nil
}