package main

import ( 
	 "fmt"
	 "encoding/csv"
	 "bufio"
	 "os"
	 "flag"
	 "os/signal"
	 "time"
)


func main() {
	var quizTimer *time.Timer
	var problemFile string
	var duration int
	ch := make(chan os.Signal)
	flag.StringVar(&problemFile, "problemFile", "problems.csv", "Adres wlasnego pliku z pytaniami")
	flag.IntVar(&duration, "duration", 30, "Czas trwania quizu")
    flag.Parse()

   	file, _ := os.Open(problemFile)
   	reader := bufio.NewReader(file)
   	csvReader := csv.NewReader(reader)
   	records, _:= csvReader.ReadAll()

   	quizTimer = time.NewTimer(time.Second*time.Duration(duration))
   	go timer(quizTimer, ch)
   	quiz(records, quizTimer, ch)
}

func quiz(records [][]string, quizTimer *time.Timer, ch chan os.Signal){
	signal.Notify(ch, os.Interrupt)
	result := 0
	for index, value := range records{
		select {
		case sig, _ := <-ch:
			if sig == os.Interrupt{
				quizTimer.Stop()
				fmt.Printf("Wynik to %d/%d", result, len(records))
				return
			}
		default:
			fmt.Printf("%d. %s = ", index+1, value[0])
			var input string
			fmt.Scanln(&input)
			if input == value[1]{
				result++
			}
		}
	}
	quizTimer.Stop()
	fmt.Printf("Wynik to %d/%d", result, len(records))
}

func timer(quizTimer *time.Timer, ch chan os.Signal){
	<-quizTimer.C
	fmt.Println("\nCzas minal!")
	ch<-os.Interrupt
}